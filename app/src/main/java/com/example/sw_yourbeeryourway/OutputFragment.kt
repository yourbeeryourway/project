package com.example.sw_yourbeeryourway

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.sw_yourbeeryourway.databinding.FragmentOutputBinding

// TODO: Break This Fragment Into Specific Output Fragments in MVVM For Individual and Brewery
class OutputFragment : Fragment() {

    lateinit var binding: FragmentOutputBinding

    val args: OutputFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentOutputBinding.inflate(this.layoutInflater, container, false)

        binding.beerNameLabel.text = "${args.beerObject.name}"
        binding.beerDescLabel.text = "${args.beerObject.desc}"

        if (args.beerObject.id == "0") {
            binding.beerImgLabel.setImageResource(R.mipmap.molson_fireside)
        } else if (args.beerObject.id == "1") {
            binding.beerImgLabel.setImageResource(R.mipmap.molson_canadian)
        } else if (args.beerObject.id == "2") {
            binding.beerImgLabel.setImageResource(R.mipmap.bayside_brewing)
        } else {
            binding.beerImgLabel.setImageResource(R.mipmap.hoegaarden)
        }

        binding.btnToMainMenu.setOnClickListener {

            val actionHome = OutputFragmentDirections.actionOutputFragmentToMenuFragment()

            findNavController().navigate(actionHome)

        }

        return binding.root
    }
}