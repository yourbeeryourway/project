package com.example.sw_yourbeeryourway

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

// General About Dialog
class MainMenuAboutDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireActivity())
            .setTitle(R.string.app_name)
            .setMessage(R.string.about)
            .setPositiveButton(android.R.string.ok,null)
            .create()
    }

}