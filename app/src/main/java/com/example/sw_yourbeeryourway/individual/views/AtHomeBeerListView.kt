package com.example.sw_yourbeeryourway.individual.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.sw_yourbeeryourway.individual.views.AtHomeBeerListViewDirections
import com.example.sw_yourbeeryourway.databinding.FragmentAtHomeBeerListBinding

class AtHomeBeerListView : Fragment() {

    lateinit var binding: FragmentAtHomeBeerListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentAtHomeBeerListBinding.inflate(this.layoutInflater, container, false)

        binding.btnToMainMenu.setOnClickListener {

            val actionHome = AtHomeBeerListViewDirections.actionAtHomeBeerListToMenuFragment()

            findNavController().navigate(actionHome)
        }

        binding.btnToLikeDislike.setOnClickListener {

            val action = AtHomeBeerListViewDirections.actionAtHomeBeerListToAtHomeLikeDislike()

            findNavController().navigate(action)
        }

        return binding.root
    }
}