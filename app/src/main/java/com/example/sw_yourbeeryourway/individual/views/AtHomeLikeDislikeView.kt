package com.example.sw_yourbeeryourway.individual.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.sw_yourbeeryourway.individual.views.AtHomeLikeDislikeViewDirections
import com.example.sw_yourbeeryourway.databinding.FragmentAtHomeLikeDislikeBinding

class AtHomeLikeDislikeView : Fragment() {

    lateinit var binding: FragmentAtHomeLikeDislikeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentAtHomeLikeDislikeBinding.inflate(this.layoutInflater, container, false)

        binding.btnToOutput.setOnClickListener {

            /*val action = AtHomeLikeDislikeViewDirections.actionAtHomeLikeDislikeToOutputFragment()

                findNavController().navigate(action)*/
        }

        return binding.root
    }

}