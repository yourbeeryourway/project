package com.example.sw_yourbeeryourway.brewery.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.sw_yourbeeryourway.databinding.FragmentBreweryInfoBinding

class BreweryInfoView : Fragment() {

    lateinit var binding: FragmentBreweryInfoBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentBreweryInfoBinding.inflate(this.layoutInflater, container, false)

        binding.btnToMainMenu.setOnClickListener {

           val actionHome = BreweryInfoViewDirections.actionBreweryInfoToMenuFragment()

            findNavController().navigate(actionHome)

        }

        binding.btnToQuiz.setOnClickListener {

            val action = BreweryInfoViewDirections.actionBreweryInfoToInputFragment()

            findNavController().navigate(action)

        }

        return binding.root
    }
}