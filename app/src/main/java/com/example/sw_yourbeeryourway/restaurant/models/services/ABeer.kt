package com.example.sw_yourbeeryourway.restaurant.models.services

import android.os.Parcel
import android.os.Parcelable

data class ABeer(var id:String, var name:String, var desc:String): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(desc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ABeer> {
        override fun createFromParcel(parcel: Parcel): ABeer {
            return ABeer(parcel)
        }

        override fun newArray(size: Int): Array<ABeer?> {
            return arrayOfNulls(size)
        }
    }
}
