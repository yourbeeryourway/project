package com.example.sw_yourbeeryourway.restaurant.models

import com.example.sw_yourbeeryourway.restaurant.models.services.ABeer
import com.example.sw_yourbeeryourway.restaurant.models.services.SwapApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import java.lang.Exception

class RestaurantOutputFragmentModel(beerValue : String) {

    private var beerValueVar : String = beerValue
    public fun callBeer(){
        CoroutineScope(Dispatchers.Main).launch {
            try{
                val response = SwapApi.retrofitService.getABeerFromAPI(beerValueVar)

                if(response.isSuccessful && response.body() != null){

                    var beer = response
                }
            }
            catch(e:Exception){

            }
        }
    }


    public fun getBeerList() : List<ABeer>{
        lateinit var beers: Response<List<ABeer>>
        CoroutineScope(Dispatchers.Main).launch {
            try{
                val response = SwapApi.retrofitService.getAllBeersFromAPI()

                if(response.isSuccessful && response.body() != null){

                    var beers = response.body()!!

                }
            }
            catch(e:Exception){

            }
        }
        return beers.body()!!
    }
}