package com.example.sw_yourbeeryourway.restaurant.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sw_yourbeeryourway.restaurant.models.RestaurantOutputFragmentModel
import com.example.sw_yourbeeryourway.restaurant.models.services.ABeer
import com.example.sw_yourbeeryourway.restaurant.models.services.SwapApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import java.lang.Exception

class RestaurantOutputFragmentViewModel : ViewModel() {
    lateinit var model: RestaurantOutputFragmentModel

    private var beerValueVar: String = ""
    public var beer : ABeer = ABeer("","","")

    public fun callBeer() {
        viewModelScope.launch {
            try {
                Log.d("ABX", "looooooogggggggggggggggggg")
                val response = SwapApi.retrofitService.getABeerFromAPI(beerValueVar)
                Log.d("ABX", response.body().toString())
                if (response.isSuccessful && response.body() != null) {
                    beer = response.body()!!
                    Log.d("ABX", response.toString())

                }
            } catch (e: Exception) {
                Log.d("ABX", e.toString())
            }
        }
    }


    public fun getBeerList(): List<ABeer> {
        var beers: Response<List<ABeer>>? = null
        viewModelScope.launch {
            try {
                val response = SwapApi.retrofitService.getAllBeersFromAPI()

                if (response.isSuccessful && response.body() != null) {

                    var beers = response.body()!!

                }
            } catch (e: Exception) {

            }
        }
        return beers?.body()!!
    }

    fun setBeer(beerValue: String) {
        beerValueVar = beerValue
    }

    fun setModel(sum: String) {
        model = RestaurantOutputFragmentModel(sum)
    }
}