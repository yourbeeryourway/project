package com.example.sw_yourbeeryourway.restaurant.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.sw_yourbeeryourway.R
import com.example.sw_yourbeeryourway.databinding.FragmentRestaurantOutputViewBinding
import com.example.sw_yourbeeryourway.databinding.FragmentRestaurantQuizViewBinding
import com.example.sw_yourbeeryourway.restaurant.viewmodels.RestaurantQuizFragmentViewModel


class RestaurantQuizFragmentView : Fragment() {

    lateinit var binding: FragmentRestaurantQuizViewBinding

    private val vm:RestaurantQuizFragmentViewModel by viewModels()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        binding = FragmentRestaurantQuizViewBinding.inflate(this.layoutInflater, container, false)


        binding.submitBtn.setOnClickListener {
            vm.runAlgorithm(binding.sliderSweet.progress, binding.sliderBitter.progress, binding.sliderCitrus.progress, binding.sliderSour.progress)
            vm.getSumFromModel()
            val bundle = bundleOf("algoirthmResult" to vm.sum)
            findNavController().navigate(R.id.action_restaurantQuizFragmentView_to_restaurantOutputFragmentView, bundle)

        }
        return binding.root
    }
}