package com.example.sw_yourbeeryourway.restaurant.models.services

import com.google.firebase.ktx.Firebase
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

//private const val BASE_URL = "https://sleepy-ridge-52824.herokuapp.com/"
private const val BASE_URL = "https://beerlist-9b606-default-rtdb.firebaseio.com/"

private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

private fun getHttpClient(): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY
    val httpClient = OkHttpClient.Builder()
    httpClient.addInterceptor(logging)
    return httpClient.build()
}

private val retrofit = Retrofit.Builder().addConverterFactory(MoshiConverterFactory.create(moshi)).baseUrl(
    BASE_URL).client(getHttpClient()).build()


interface ApiService {
    //@GET("api/{beerId}")
    @GET("beers/{id}.json")
    suspend fun getABeerFromAPI(@Path("id") id : String): Response<ABeer>


    //@GET("api/beerlist")
    @GET("beers.json")
    suspend fun getAllBeersFromAPI() : Response<List<ABeer>>
}

object SwapApi{
    val retrofitService : ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }
}