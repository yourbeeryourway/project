package com.example.sw_yourbeeryourway.restaurant.models

class RestaurantQuizFragmentModel {

    private var sum : Int = 0

    fun sumAlgorithm(sweetness : Int, bitterness : Int, citrus : Int, sour : Int){
         sum = sweetness + bitterness + citrus + sour
    }

    fun getSum() : Int{
        return sum
    }



}