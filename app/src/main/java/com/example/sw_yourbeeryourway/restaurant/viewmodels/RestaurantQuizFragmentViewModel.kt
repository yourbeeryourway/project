package com.example.sw_yourbeeryourway.restaurant.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import com.example.sw_yourbeeryourway.restaurant.models.RestaurantQuizFragmentModel

class RestaurantQuizFragmentViewModel: ViewModel(){

    lateinit var model : RestaurantQuizFragmentModel

    public var sum : String = ""

    fun runAlgorithm(sweetness:Int, bitterness:Int, citrus:Int, sour:Int){

        model = RestaurantQuizFragmentModel()

        model.sumAlgorithm(sweetness,bitterness,citrus,sour)

    }

    fun getSumFromModel(){
        sum = model.getSum().toString()
    }

}
