package com.example.sw_yourbeeryourway.restaurant.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.sw_yourbeeryourway.OutputFragmentDirections
import com.example.sw_yourbeeryourway.R
import com.example.sw_yourbeeryourway.databinding.FragmentRestaurantOutputViewBinding
import com.example.sw_yourbeeryourway.restaurant.viewmodels.RestaurantOutputFragmentViewModel
import com.example.sw_yourbeeryourway.restaurant.viewmodels.RestaurantQuizFragmentViewModel


class RestaurantOutputFragmentView : Fragment() {

    lateinit var binding: FragmentRestaurantOutputViewBinding

    private val vm: RestaurantOutputFragmentViewModel by viewModels()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        binding = FragmentRestaurantOutputViewBinding.inflate(this.layoutInflater, container, false)
        //vm.setModel(arguments?.getInt("algoirthmResult")!!)
        vm.setBeer(arguments?.getString("algoirthmResult")!!)
        vm.callBeer()
//        vm.getBeerList()
        binding.beerName.text = vm.beer.name
        binding.beerDes.text = vm.beer.desc
        binding.btnToMainMenu.setOnClickListener {

            val actionHome = RestaurantOutputFragmentViewDirections.actionRestaurantOutputFragmentViewToMenuFragment()

            findNavController().navigate(actionHome)

        }

        return binding.root
    }
}