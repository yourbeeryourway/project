package com.example.sw_yourbeeryourway

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.sw_yourbeeryourway.databinding.FragmentInputBinding
import com.example.sw_yourbeeryourway.databinding.FragmentMenuBinding
import com.example.sw_yourbeeryourway.restaurant.models.services.ABeer


class InputFragment : Fragment() {

    //TODO: Break This Fragment Into Specific Quiz Fragments in MVVM For Individual and Brewery

    lateinit var binding: FragmentInputBinding

    lateinit var aBeer: ABeer

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = FragmentInputBinding.inflate(this.layoutInflater, container, false)

        binding.btnSelect.setOnClickListener { selectPressed() }

        /*binding.selection1.setOnClickListener{

            findNavController().navigate(action)

        }

        binding.selection2.setOnClickListener{

            findNavController().navigate(action)

        }

        binding.selection3.setOnClickListener{

            findNavController().navigate(action)

        }

        binding.selection4.setOnClickListener{

            findNavController().navigate(action)

        }*/

        return binding.root
    }

    private fun selectPressed() {
        aBeer = ABeer(id = "", name = "", desc = "")
        if (this::aBeer.isInitialized) {
            // whatever your button click handler code is
            // when the person presses the button, it goes to screen 2
            val radioButtonId = binding.rdoChoose.checkedRadioButtonId

            if (radioButtonId == binding.rdoDark.id) {
                aBeer.id = "0"
                aBeer.name = "Molson Fireside"
                aBeer.desc = "Inspired by 7 generations of brewing history using 7 specially selected malts. A light-bodied Dark Lager with notes of chocolate, caramel and roasted coffee beans. Deliciously brewed to be enjoyed with warm company and paired with favourite holiday meals."
            } else if (radioButtonId == binding.rdoLight.id) {
                aBeer.id = "1"
                aBeer.name = "Molson Canadian"
                aBeer.desc = "Made from the very best ingredients and always preservative-free. Pours a light golden colour. Delicate floral and barley aromas with notes of citrus, grain and sweet fruit. Flavours of light hops, sweet malt and citrus. Crisp, clean, medium-bodied and smooth on the palate. Great on its own or paired to grilled white meats."
            } else if (radioButtonId == binding.rdoHops.id) {
                aBeer.id = "2"
                aBeer.name = "Bayside Brewing, Honey Cream Ale"
                aBeer.desc = "Based in Erieau, Ontario, this brewery is all about crafting delightful beer. This award-winning cream ale has aromas of honey, lemon, floral hops and tea leaves. Flavours of toffee and baked bread are followed by a mildly sweet and refreshingly balanced finish. Serve well-chilled with grilled vegetable salad."
            } else {
                aBeer.id = "3"
                aBeer.name = "Hoegaarden"
                aBeer.desc = "Hoegaarden monks were the first to discover and experiment with flavouring wheat beer with orange peel and coriander. Naturally cloudy with a pale golden colour, followed by aromas and flavours of soft wheat, citrus, orange and coriander spice; delicately creamy with a smooth crisp, clean finish."
            }

            val action = InputFragmentDirections.actionInputFragmentToOutputFragment(aBeer)

            findNavController().navigate(action)
        }
    }
}