package com.example.sw_yourbeeryourway

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.sw_yourbeeryourway.databinding.FragmentMenuBinding

// General App Main Menu Dialog Provides user access to Three Subsystems
class MainMenuFragment : Fragment() {

    lateinit var binding: FragmentMenuBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // findViewById(R.id.home_selection)
        //var homeBtn = findViewById(R.id.home_selection)

        binding = FragmentMenuBinding.inflate(this.layoutInflater, container, false)

        binding.homeSelection.setOnClickListener {
            //view?.findNavController()?.navigate(R.id.action_menuFragment_to_atHomeBeerList)

            val actionHome = MainMenuFragmentDirections.actionMenuFragmentToAtHomeBeerList()

            findNavController().navigate(actionHome)

        }

        binding.brewerySelection.setOnClickListener {
            //view?.findNavController()?.navigate(R.id.action_menuFragment_to_atHomeBeerList)

            val actionBrewery = MainMenuFragmentDirections.actionMenuFragmentToBreweryInfo()

            findNavController().navigate(actionBrewery)

        }

        binding.restaurantSelection.setOnClickListener {
            //view?.findNavController()?.navigate(R.id.action_menuFragment_to_atHomeBeerList)

            val actionRestaurant = MainMenuFragmentDirections.actionMenuFragmentToInputFragment()

            findNavController().navigate(actionRestaurant)

        }

        return binding.root
    }
}